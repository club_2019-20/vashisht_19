function getnum(prompt)
    print(prompt)
    n=readline()
    parse(Int,n)
end

function isprime(n)
    i=2
    while i<=n/2+1
        if((n%i==0 && n!=2)||n==1)
            return false
        else
            i=i+1
        end
    end
    return true
end

function least(n)
    j=2
    a=1
    y=1
    while j<=n
        if(isprime(j))
            y=j
            while y*j<=n
                y=j*y          
            end
            a=a*y
        end
        j=j+1
    end
    return a
end

n=getnum("Enter: ")
print(least(n))