function fact(n)
    n == 1 ? 1 : n*fact(n-1)
end

println("Enter the number of astronauts: ")
n = parse(Int, chomp(readline(stdin)))

num = fact(n)
println("The minimum planet number is $num.")
