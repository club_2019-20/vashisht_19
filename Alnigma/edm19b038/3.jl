# ASCII art of solar system is copied from https://www.asciiart.eu/space/planets

sSys = ["                  :"
"                       :"
"                       :"
"                       :"
"        .              :"
"         \'.            :           .\'"
"           \'.          :         .\'"
"             \'.   .-\"\"\"\"\"\"-.   .\'                                   .\'\':"
"               \'.\"          \".\'                               .-\"\"\"\"-.\'         .---.          .----.        .-\"\"\"-."
"                :            :                _    _        .\"     .\' \".    ...\"     \"...    .\"      \".    .\"       \"."
"        .........            .........    o  (_)  (_)  ()   :    .\'    :   \'..:.......:..\'   :        :    :         :   o"
"                :            :                              :  .\'      :       \'.....\'       \'.      .\'    \'.       .\'"
"                 :          :                             .\'.\'.      .\'                        `\'\'\'\'`        `\'\'\'\'\'`"
"                  \'........\'                              \'\'   ``````"
"                 .\'    :   \'."
"               .\'      :     \'."
"             .\'        :       \'."
"           .\'          :         \'."
"                       :"
"                       :"
"                       :"
"                       :"]

for s in sSys println(s) end
