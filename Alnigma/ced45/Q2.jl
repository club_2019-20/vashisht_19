println("Enter number of astronauts")
N=parse(Int64,readline())
function lcm(a,b)
    p=a*b
    hcf=1
    for i in 2:p 
        if a%i==0 && b%i==0
             hcf=i;
        end
    end
    return p/hcf
end
min=2
for k in 3:N 
    global min=lcm(k,min)
end
println("Number of planets visited by all astronauts is ",trunc(Int64,min))


